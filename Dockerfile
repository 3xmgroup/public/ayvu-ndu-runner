FROM docker:latest

RUN apk --no-cache add gitlab-runner py3-pip aws-cli curl openssl git bash jq && pip install yq && mkdir /etc/gitlab-runner

RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | sh && curl -L -o /usr/local/bin/sops https://github.com/mozilla/sops/releases/download/v3.5.0/sops-v3.5.0.linux && chmod +x /usr/local/bin/sops && curl -o /usr/local/bin/aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.16.8/2020-04-16/bin/linux/amd64/aws-iam-authenticator && chmod +x /usr/local/bin/aws-iam-authenticator 

COPY config.toml /etc/gitlab-runner/config.toml

CMD gitlab-runner run

