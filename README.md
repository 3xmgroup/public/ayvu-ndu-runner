# Steps to get a runner

1. Clone the repo
2. Build the image running `docker-compose build`
3. Register the runner and generate a config.toml file
```bash
export PROJECT_REGISTRATION_TOKEN="PUTYOURTOKEN"

docker run --rm -it -v $(pwd):/app --user 1000  ayvu-ndu-runner_ayvu-runner:latest gitlab-runner register \
  --config /app/config.toml \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
  --executor "shell" \
  --description "namandu-runner" \
  --tag-list "amd64" \
  --run-untagged="false" \
  --locked="true" \
  --access-level="not_protected"
```
4. Re build the images, so valid config.toml is built into the container `docker-compose build`
4. Put the runner to work `docker-compose up -d`
